function Add-FirewallException
{
    # param([string] $port)

    # Delete an exisitng rule
    # netsh advfirewall firewall delete rule name="Windows Remote Management (HTTPS-In)" dir=in protocol=TCP localport=$port

    # Add a new firewall rule
    netsh advfirewall firewall add rule name="Allow SSH" dir=in action=allow protocol=TCP localport=22
}

Add-FirewallException